import { State, GameManager, Card } from 'badjack'

/** Console extension of GameManager */
export class ConsoleManager extends GameManager {
  /** 
   * Display the game table in console
   * @param displayHidden - if false (default) change the cards marked with attribute hidden, otherwise display them 
  */
  public displayTable (displayHidden: boolean = false) {
    const cardsLeft = this.deck.length

    if (displayHidden)
      this.dealer.showHand()

    const displayCards = (cards: Card[]) => {
      return cards.map((card) => !card.hidden ? card.symbol + card.suit : '??').join(' + ')
    }

    const playerMoney = this.player.bank.total
    const playerCards = displayCards(this.player.hand.cards)
    const dealerCards = displayCards(this.dealer.hand.cards)
    const playerScore = this.player.hand.countValue()
    const dealerScore = this.dealer.hand.countValue()

    console.log(`${playerMoney}$ in your bank || ${cardsLeft} cards left in the deck`)
    console.log(`${this.player.name}: ${playerCards} = ${playerScore}`)
    console.log(`${this.dealer.name}: ${dealerCards} = ${dealerScore}\n`)
  }

  /**
   * Display the result text of a game
   * @param state - the State value that is returned by the manager
   * @param victory - the amount of money the player won
  */
  public displayResult (state: State, victory: number) {
    this.displayTable(true)

    switch (state) {
      case State.BLACKJACK:
        console.log(`+ Blackjack!\nReceived ${victory}$\n`)
        break
      case State.WIN:
        console.log(`+ You won!\nReceived ${victory}$\n`)
        break
      case State.LOST:
        console.log(`- Dealer won!\n`)
        break
      case State.PUSH:
        console.log('- Push!\n')
        break
      case State.PLAYING:
        console.log('The game is still going on for some reason')
        break
    }
  }

  /**
   * A function handling user's input from the input library
   * @param {string} input - the choice the player has chosen
   * @returns a boolean saying if the game is finished or not
   * @memberof ConsoleManager
  */
  public handleInput (input: string) : boolean {
    if (input === 'submit') {
      const bet = this.player.bank.hold
      const state = this.submitHand()

      const victory = this.processState(state)
      this.displayResult(state, victory)
      return true
    }

    const state = this.draw()

    if (state !== State.PLAYING) {
      const victory = this.processState(state)
      this.displayResult(state, victory)
      return true
    }

    return false
  }
}
