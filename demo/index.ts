import { select } from 'input'
import { State } from 'badjack'
import { ConsoleManager } from './ConsoleManager'

const manager = new ConsoleManager()
main()

/** Main function that's ran after initiating the manager, it handles the game and the player's input */
async function main () {
  console.clear()
  manager.createNewGame(100)

  while (true) {
    manager.displayTable()

    const action = await select('What will you do?', [ 'draw', 'submit' ])
    console.clear()

    const isFinished = manager.handleInput(action)

    if (isFinished)
      await askForRestart(manager)
  }
}

/**
 * Ask if the player wants to play again, start a new game if they do, otherwise close the process
 * @param manager - ConsoleManager instance of the current game
*/
async function askForRestart (manager: ConsoleManager): Promise<State> {
  const result = await select('Try again?', [ 'yes', 'no' ]) 

  if (result === 'yes') {
    console.clear()
    return manager.createNewGame(100)
  }

  process.exit()
}
