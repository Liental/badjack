import { Card } from './Card'
import { State } from './State'
import { Player, Dealer } from './Player'
import { Multiplier } from './Multiplier'
import { DeckManager } from './DeckManager'

/** Object managing the game state and its players */
export class GameManager {
  /** The deck of cards used in the game */
  public deck: Card[] = [ ]

  /** Player object used for the game */
  public player: Player = new Player()

  /** Extension of Player class used for the dealer */
  public dealer: Dealer = new Dealer()

  /**
   * Create new game with given amount of decks, shuffle in the deck and make players draw two cards
   * @param bet - amount of money to put on bet
   * @param decksNumber - number of decks to shuffle in
   * @returns the current state of the game
  */
  public createNewGame (bet: number, decksNumber: number = 4): State {
    if (this.deck.length < 10) {
      const deck = DeckManager.generateDecks(decksNumber)
      this.deck = DeckManager.shuffleDeck(deck)
    }

    this.player.hand.restart(this.deck)
    this.dealer.hand.restart(this.deck)
    this.player.bank.setHold(bet)

    return this.checkState()
  }

  /** 
   * Draw a card from given deck, check for the victory and return the result
   * @returns the current state of the game
  */
  public draw () : State {
    this.player.hand.draw(this.deck) 
    return this.checkState()
  }

  /**
   * Checks the state of the game and returns the result
   * @param submited - player submited their hand
   * @returns the current state of the game
  */
  public checkState (submited: boolean = false) : State {
    const value = this.player.hand.countValue()
    const dealerValue = this.dealer.hand.countValue()

    if (submited && this.player.hand.cards.length === 2 && value === 21) 
      return State.BLACKJACK

    if (submited && (value > dealerValue || dealerValue > 21))
      return State.WIN

    if (submited && value === dealerValue)
      return State.PUSH

    if (value > 21 || (submited && value < dealerValue))
      return State.LOST

    return State.PLAYING
  }

  /** 
   * Transfer money according to the state
   * @param state - the state of the game
   * @returns a number that represents the money won by the player
  */
  public processState (state: State) : number {
    switch (state) {
      case State.BLACKJACK:
        return this.player.bank.transferHold(Multiplier.BLACKJACK)
      case State.WIN:
        return this.player.bank.transferHold()
      case State.PUSH:
        return this.player.bank.transferHold(Multiplier.PUSH)
      case State.LOST:
        return this.player.bank.removeHold()
      case State.PLAYING:
        return 0
    }
  }

  /**
   * Submit player's hand, make dealer draw cards and return the state of the game
   * @returns the state of the game after the dealer drawn their cards  
  */
  public submitHand () : State {
    this.dealer.hand.draw(this.deck)
    return this.checkState(true)
  }
}