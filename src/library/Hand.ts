import { CardList, Card } from './Card'

/** Hand object that the players use to store and manage the cards */
export class Hand {
  /** Set of cards on the hand */
  public cards: Card[] = [ ]

  /** 
   * Count the value of cards in the hand and return it
   * @returns a value of all cards in the hand
  */
  public countValue () : number {
    let value = 0
    const sorted = CardList.sortByValue(this.cards)

    for (const card of sorted) {
      if (!card.hidden)
        value += card.value(value)
    }

    return value
  }

  /** 
   * Reset the hand and add two cards to it
   * @param deck - deck of cards to draw from
  */
  public restart (deck: Card[]) {
    this.cards = [ deck.pop(), deck.pop() ]
  }

  /**
   * Draw a card from the deck
   * @param deck - deck of cards to draw from
  */
  public draw (deck: Card[]) {
    this.cards.push(deck.pop())
  }
}

/** Hand object that the dealer uses to manage the cards */
export class DealerHand extends Hand {
  /** Reset the hand and add two cards to it */
  public restart (deck: Card[]) {
    this.cards = [ deck.pop(), deck.pop() ]
    this.cards[1].hidden = true    
  }

  /**
   * Draw the cards as long as the value is below 17
   * @param deck - deck of cards to draw from
   */
  public draw (deck: Card[]) {
    this.cards[1].hidden = false

    while (this.countValue() < 17) {
      const card = deck.pop()

      card.hidden = false
      this.cards.push(card)
    }
  }
}
