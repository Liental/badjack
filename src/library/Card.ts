/** French deck suits used by cards */
export enum Suit {
  SUIT_CLUBS = '♣',
  SUIT_HEARTS = '♥',
  SUIT_SPADES = '♠',
  SUIT_DIAMONDS = '♦'
}

/** Interfact used to represent cards */
export interface Card {
  /** The symbol that the card is representing */
  symbol: string
  
  /** Is the card hidden from the player */
  hidden?: boolean

  /** The suit that the card wears*/
  suit?: Suit

  /**
   * Function to get the value of the card
   * @param v - value of the current hand (used only by the Ace card)
  */
  value: (v?: number) => number
}

/** Class containing all the cards */
export class CardList {
  public static readonly CARD_TWO: Card = { symbol: '2', value: () => 2 }
  public static readonly CARD_THREE: Card = { symbol: '3', value: () => 3 }
  public static readonly CARD_FOUR: Card = { symbol: '4', value: () => 4 }
  public static readonly CARD_FIVE: Card = { symbol: '5', value: () => 5 }
  public static readonly CARD_SIX: Card = { symbol: '6', value: () => 6 }
  public static readonly CARD_SEVEN: Card = { symbol: '7', value: () => 7 }
  public static readonly CARD_EIGHT: Card = { symbol: '8', value: () => 8 }
  public static readonly CARD_NINE: Card = { symbol: '9', value: () => 9 }
  public static readonly CARD_TEN: Card = { symbol: '10', value: () => 10 }
  public static readonly CARD_JACK: Card = { symbol: 'J', value: () => 10 }
  public static readonly CARD_QUEEN: Card = { symbol: 'Q', value: () => 10 }
  public static readonly CARD_KING: Card = { symbol: 'K', value: () => 10 }
  public static readonly CARD_ACE: Card = { symbol: 'A', value: (v: number = 0) => (v + 11 <= 21 ? 11 : 1) }

  /** All the cards without their symbol defined */
  public static readonly ALL_CARDS: Card[] = [
    CardList.CARD_TWO, CardList.CARD_THREE, CardList.CARD_FOUR, CardList.CARD_FIVE,
    CardList.CARD_SIX, CardList.CARD_SEVEN, CardList.CARD_EIGHT, CardList.CARD_NINE, CardList.CARD_TEN,
    CardList.CARD_JACK, CardList.CARD_QUEEN, CardList.CARD_KING, CardList.CARD_ACE
  ]

  /**
   * Sorts given cards by their value from the lowest to the highest
   * @param cards - cards to sort
   * @returns provided deck of cards sorted by their value
   */
  public static sortByValue (cards: Card[]) : Card[] {
    const copied = cards.slice()
    const sorted = copied.sort((a: Card, b: Card) => {
      const aReadonly = CardList.ALL_CARDS.filter((card) => card.symbol === a.symbol)[0]
      const bReadonly = CardList.ALL_CARDS.filter((card) => card.symbol === b.symbol)[0]

      const aIndex = CardList.ALL_CARDS.indexOf(aReadonly)
      const bIndex = CardList.ALL_CARDS.indexOf(bReadonly)

      return aIndex > bIndex ? 1 : -1
    })
    
    return sorted
  }
}
