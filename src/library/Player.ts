import { Bank } from './Bank'
import { Card, CardList } from './Card'
import { Hand, DealerHand } from './Hand'

/** Player class representing an user in the game */
export class Player {
  /** Display name for the player */
  public name: string = 'Player'

  /** Set of cards on hand */
  public hand: Hand = new Hand()

  /** Private bank of the user containing all the money */
  public bank: Bank = new Bank()
}

/** Player class extension representing the dealer in the game */
export class Dealer extends Player {
  public name: string = 'Dealer'
  public hand: Hand = new DealerHand()

  /** Set all cards in hand to be shown */
  public showHand () {
    for (const card of this.hand.cards) 
      card.hidden = false
  }
}