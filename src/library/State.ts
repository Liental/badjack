/** Enum containing states of the game */
export enum State {
  /** The player has submitted the hand and have won */
  WIN,

  /** The player had lower score than the dealer or greater score than 21 */
  LOST,

  /** The player had the same score as the dealer */
  PUSH,

  /** The game is still going */
  PLAYING,

  /** The player had a score of 21 with the two first cards */
  BLACKJACK
}
