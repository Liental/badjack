/** Multiplier enum containing a list of multipliers for given win/lose situation */
export enum Multiplier {
  /** Multiplier for when the user won */
  WIN = 2,

  /** Multiplier for when the user had the same score as the dealer */
  PUSH = 1,

  /** Multiplier for when the user lost */
  LOST = 0,

  /** Multiplier for when the user got a blackjack */
  BLACKJACK = 2.5
}
