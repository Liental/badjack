import { Multiplier } from './Multiplier'

/** Class managing money of the user */
export class Bank {
  /** The amount of money the user put for the bet */
  public hold: number = 0

  /** The total amount of money the user posseses */
  public total: number = 5000

  /**
   * Set user's hold to the given amount, amount can't be smaller than the user's total amount of money
   * @param amount - amount of money to put on hold
   * @returns a boolean defining if the hold has been set or not
  */
  public setHold (amount: number) : boolean {
    if (amount <= this.total) {
      this.hold = amount
      this.total -= amount
      return true
    }
    
    return false
  }

  /** 
   * Remove the hold from player's total amount of money and returns the hold
   * @returns a number saying how much money did the player win
  */
  public removeHold () : number {
    return this.transferHold(Multiplier.LOST)
  }

  /** 
   * Add the hold to the player's total amount of money with given multiplier and returns the multiplied value
   * @param multiplier - how many time the hold should be multiplied
   * @returns a number saying how much money did the player win
  */
  public transferHold (multiplier: Multiplier | number = Multiplier.WIN) : number {
    const result = this.hold * multiplier
    
    this.hold = 0
    this.total += result
  
    return result
  }
}
