import { Card, CardList, Suit } from './Card'

/** Static object used to handle deck operations */
export class DeckManager {
  /** 
   * Generate one deck of cards with the ALL_CARDS constant 
   * @returns a deck of 52 cards
  */
  public static generateDeck () : Card[] {
    return CardList.ALL_CARDS
           .map((card: Card) => [ 
              { symbol: card.symbol, value: card.value, suit: Suit.SUIT_CLUBS }, 
              { symbol: card.symbol, value: card.value, suit: Suit.SUIT_HEARTS }, 
              { symbol: card.symbol, value: card.value, suit: Suit.SUIT_SPADES }, 
              { symbol: card.symbol, value: card.value, suit: Suit.SUIT_DIAMONDS }
            ])
           .reduce((a, b) => a.concat(b))
  }
  
  /**
   * Generate a deck given number of times and return the result
   * @param decksNumber - number of decks to generate
   * @returns X decks of 52 cards
  */
  public static generateDecks (decksNumber: number = 4) {
    let cards = [ ]

    for (let i = 0; i < decksNumber; i++) {
      const deck = DeckManager.generateDeck()
      cards = cards.concat(deck)
    }

    return cards
  }

  /**
   * Shuffle given deck using Fisher-Yates shuffle and return the result
   * @param deck - the deck to shuffle 
   * @returns a shuffled deck of provided cards
  */
  public static shuffleDeck (deck: Card[]) : Card[] {
    const result: Card[] = [ ]
    const clone = deck.slice()

    while (clone.length > 0) {
      const random = Math.floor(Math.random() * clone.length)
      const card = clone.splice(random, 1)
      
      result.push(card[0])
    }

    return result
  }
}
