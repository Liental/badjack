import { Bank } from '../library/Bank'
import { Multiplier } from '../library/Multiplier'

describe ('#Bank', () => {
  let bank: Bank = new Bank()

  beforeEach(() => {
    bank = new Bank()
  })

  test('should accept the bet and transfer 5000 from the total to the hold', () => {
    const result = bank.setHold(5000)
    
    expect(result).toBeTruthy()
    expect(bank.hold).toBe(5000)
    expect(bank.total).toBe(0)
  })

  test('should deny the bet and not transfer anything to the total', () => {
    const result = bank.setHold(15000)

    expect(result).toBeFalsy()
    expect(bank.hold).toBe(0)
    expect(bank.total).toBe(5000)
  })

  test('should transfer victory state money into player\'s total bank', () =>{
    bank.setHold(500)
    const result = bank.transferHold()

    expect(result).toBe(1000)
    expect(bank.total).toBe(5500)
  })

  test('should transfer blackjack state money into player\'s total bank', () =>{
    bank.setHold(500)
    const result = bank.transferHold(Multiplier.BLACKJACK)

    expect(result).toBe(1250)
    expect(bank.total).toBe(5750)
  })

  test('should give back the money to the player\'s total bank', () =>{
    bank.setHold(500)
    const result = bank.transferHold(Multiplier.PUSH)

    expect(result).toBe(500)
    expect(bank.total).toBe(5000)
  })

  test('should remove player\'s hold', () =>{
    bank.setHold(500)
    const result = bank.removeHold()

    expect(result).toBe(0)
    expect(bank.total).toBe(4500)
  })
})