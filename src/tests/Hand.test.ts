import { Hand, DealerHand } from '../library/Hand'
import { Card, CardList } from '../library/Card'

describe ('#Hand', () => {
  // The deck used for the tests
  let deck: Card[] = [ ]
  
  // Hand object used in the tests
  let hand = new Hand()

  // Refill the deck and create a new hand before every test
  beforeEach(() => {
    hand = new Hand()
    deck = CardList.ALL_CARDS.slice()
  })
  
  test('counts the value of a revealed Ace and a revealed Queen', () => {
    hand.cards = [ CardList.CARD_ACE, CardList.CARD_QUEEN ]

    expect(hand.countValue()).toBe(21)
  })
  
  test('counts the value of a revealed Ace and a revealed Queen', () => {
    hand.cards = [ CardList.CARD_ACE, CardList.CARD_QUEEN ]
    hand.cards[1].hidden = true

    expect(hand.countValue()).toBe(11)
  })
  
  test('should empty the hand and pop 2 new cards from the deck', () => {
    const expected = [ deck[deck.length - 1], deck[deck.length - 2] ]
    hand.cards = [ CardList.CARD_EIGHT, CardList.CARD_FIVE ]

    hand.restart(deck)
    expect(deck.length).toBe(11)
    expect(hand.cards).toStrictEqual(expected)
  })

  test('should draw a card from the top of the deck', () => {
    const expected = [ deck[deck.length - 1] ]

    hand.draw(deck)
    expect(deck.length).toBe(12)
    expect(hand.cards).toStrictEqual(expected)
  })
})

describe ('#DealerHand', () => {
  // The deck used for the tests
  let deck: Card[] = [ ]

  // Hand object used in the tests
  let hand = new DealerHand()

  // Refill the deck and create a new hand before every test
  beforeEach(() => {
    hand = new DealerHand()
    deck = CardList.ALL_CARDS.slice()
  })

  test('should empty the hand and pop 2 new cards from the deck and hide the 2nd one', () => {
    const expected = [ CardList.CARD_ACE, CardList.CARD_KING ]
    
    expected[1].hidden = true
    hand.cards = [ CardList.CARD_EIGHT, CardList.CARD_FIVE ]
    
    hand.restart(deck)
    expect(deck.length).toBe(11)
    expect(hand.cards).toStrictEqual(expected)
  })

  test('should reveal the 2nd card in the deck and start drawing', () => {
    const expected = [ CardList.CARD_EIGHT, CardList.CARD_THREE, CardList.CARD_ACE, CardList.CARD_KING ]

    hand.cards = [ CardList.CARD_EIGHT, CardList.CARD_THREE ]
    hand.cards[1].hidden = true
    hand.draw(deck)
    
    expect(hand.cards).toStrictEqual(expected)
  })
})