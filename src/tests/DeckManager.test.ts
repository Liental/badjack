import { CardList } from '../library/Card'
import { DeckManager } from '../library/DeckManager'

describe ('#DeckManager', () => {
  test('should generate a single deck containing 52 cards', () => {
    const deck = DeckManager.generateDeck()

    for (const card of CardList.ALL_CARDS) {
      const cards = deck.filter((c) => c.symbol === card.symbol)
      expect(cards.length).toBe(4)
    }

    expect(deck.length).toBe(52)
  })

  test('should generate three decks of 52 cards', () => {
    const deck = DeckManager.generateDecks(3)
    expect(deck.length).toBe(156)
  })

  test('should generate four decks of 52 cards', () => {
    const deck = DeckManager.generateDecks()
    expect(deck.length).toBe(208)
  })

  test('should shuffle the deck and return the result', () => {
    const deck = DeckManager.generateDeck()
    const shuffled = DeckManager.shuffleDeck(deck)

    for (const card of CardList.ALL_CARDS) {
      const cards = shuffled.filter((c) => c.symbol === card.symbol)
      expect(cards.length).toBe(4)
    }
  })
})