import { Dealer } from '../library/Player'
import { CardList } from '../library/Card'

describe ('#Dealer', () => {
  test('should set all the cards to revealed', () => {
    const dealer = new Dealer()
    
    dealer.hand.cards = [ CardList.CARD_EIGHT, CardList.CARD_FIVE ]
    dealer.hand.cards[0].hidden = true

    dealer.showHand()
    expect(dealer.hand.cards[0].hidden).toBeFalsy()
  })
})
