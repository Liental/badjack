import { CardList } from '../library/Card'

describe('#Card', () => {
  test('should make an ace become an one if the value is above to 21, all the other cards should stay the same', () => {
    const result = CardList.ALL_CARDS.map((card) => card.value(20))
    const expected = [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 1 ]
  
    expect(result).toStrictEqual(expected)
  })
  
  test('should make an ace become an eleven if the value is below or equal to 21, all the other cards should stay the same', () => {
    const result = CardList.ALL_CARDS.map((card) => card.value(10))
    const expected = [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11 ]
  
    expect(result).toStrictEqual(expected)
  })

  test ('should default current hand to 0 and make an ace an eleven', () => {
    const result = CardList.ALL_CARDS.map((card) => card.value())
    const expected = [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11 ]
  
    expect(result).toStrictEqual(expected)
  })
  
  test('should sort all the cards by their value and return them', () => {
    const cards = [ CardList.CARD_ACE, CardList.CARD_EIGHT, CardList.CARD_JACK, CardList.CARD_NINE ]
    const expected = [ CardList.CARD_EIGHT, CardList.CARD_NINE, CardList.CARD_JACK, CardList.CARD_ACE ]
    const result = CardList.sortByValue(cards)
  
    expect(result).toStrictEqual(expected)
  })
})