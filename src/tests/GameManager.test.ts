import { State } from '../library/State'
import { CardList } from '../library/Card'
import { GameManager } from '../library/GameManager'

describe ('#GameManager', () => {
  let manager: GameManager

  beforeEach(() => {
    manager = new GameManager()
  })

  test('should create a new game with a bet of 500 and return its state', () => {
    const result = manager.createNewGame(500)

    expect(result).toBe(State.PLAYING)
    expect(manager.deck.length).toBe(204)
    expect(manager.player.bank.hold).toBe(500)
    expect(manager.player.hand.cards.length).toBe(2)
    expect(manager.dealer.hand.cards.length).toBe(2)
  })

  test('should create a new game with a bet of 500 not refill', () => {
    manager.deck = CardList.ALL_CARDS.slice()
    const result = manager.createNewGame(500)

    expect(result).toBe(State.PLAYING)
    expect(manager.deck.length).toBe(9)
    expect(manager.player.bank.hold).toBe(500)
    expect(manager.player.hand.cards.length).toBe(2)
    expect(manager.dealer.hand.cards.length).toBe(2)
  })
  
  test('should create a new game with a bet of 500 and 6 decks of cards', () => {
    const result = manager.createNewGame(500, 6) 
    expect(manager.deck.length).toBe(308)
  })

  test('should draw a card and return the state of the game', () => {
    manager.createNewGame(0)
    const result = manager.draw()

    expect(result).toBeDefined()
    expect(manager.player.hand.cards).toHaveLength(3)
  })

  test('should check the state of the game and act for a blackjack situation', () => {
    manager.player.bank.setHold(500)
    manager.player.hand.cards = [ CardList.CARD_ACE, CardList.CARD_JACK ]
    manager.player.hand.cards[1].hidden = false

    const result = manager.checkState(true)

    expect(result).toBe(State.BLACKJACK)
  })

  test('should check the state of the game and act for a win situation', () => {
    manager.player.bank.setHold(500)
    manager.dealer.hand.cards = [ CardList.CARD_KING, CardList.CARD_SEVEN ]
    manager.player.hand.cards = [ CardList.CARD_KING, CardList.CARD_JACK ]
    manager.player.hand.cards[0].hidden = false
    manager.player.hand.cards[1].hidden = false
    manager.dealer.showHand()

    const result = manager.checkState(true)

    expect(result).toBe(State.WIN)
  })

  test('should check the state of the game and act for a lose situation', () => {
    manager.player.bank.setHold(500)
    manager.dealer.hand.cards = [ CardList.CARD_KING, CardList.CARD_SEVEN ]
    manager.player.hand.cards = [ ]
    manager.dealer.showHand()

    const result = manager.checkState(true)

    expect(result).toBe(State.LOST)
  })

  test('should check the state of the game and act for a push situation', () => {
    manager.player.bank.setHold(500)
    manager.dealer.hand.cards = [ CardList.CARD_KING, CardList.CARD_SEVEN ]
    manager.player.hand.cards = [ CardList.CARD_KING, CardList.CARD_SEVEN ]
    manager.dealer.showHand()

    const result = manager.checkState(true)

    expect(result).toBe(State.PUSH)
  })

  test('should submit the hand and return the result', () => {
    manager.createNewGame(500)
    const result = manager.submitHand()

    expect(result).toBeDefined()
    expect(manager.dealer.hand.cards[1].hidden).toBeFalsy()
    expect(manager.dealer.hand.cards.length).toBeGreaterThanOrEqual(2)
  })

  test('should process the BLACKJACK state and return the result', () => {
    manager.player.bank.setHold(500)
    const result = manager.processState(State.BLACKJACK)
    
    expect(result).toBe(1250)
  })
  
  test('should process the WIN state and return the result', () => {
    manager.player.bank.setHold(500)
    const result = manager.processState(State.WIN)
    
    expect(result).toBe(1000)
  })

  test('should process the LOST state and return the result', () => {
    manager.player.bank.setHold(500)
    const result = manager.processState(State.LOST)
    
    expect(result).toBe(0)
  })

  test('should process the PUSH state and return the result', () => {
    manager.player.bank.setHold(500)
    const result = manager.processState(State.PUSH)
    
    expect(result).toBe(500)
  })
  
  test('should process the PLAYING state and return the result', () => {
    manager.player.bank.setHold(500)
    const result = manager.processState(State.PLAYING)
    
    expect(result).toBe(0)
  })
})
