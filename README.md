[![pipeline status](https://gitlab.com/Liental/badjack/badges/master/pipeline.svg)](https://gitlab.com/Liental/badjack/commits/master)
[![coverage report](https://gitlab.com/Liental/badjack/badges/master/coverage.svg)](https://gitlab.com/Liental/badjack/commits/master)

# Badjack

## About
BadJack is a NodeJS blackjack library, it’s in its early stages of development and it does not have all the features yet, but the development is ongoing. You try out a console implementation demo from the demo directory!
